﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnController : MonoBehaviour
{
    CardHandlers cardHandler;
    ValueController valueCntrllr;

    public bool algorithm1Turn;
    public bool algorithm2Turn;

    int turnCount;
    int whoFirst;

    // Start is called before the first frame update
    
    void Start()
    {
        cardHandler = GameObject.Find("CardHandler").GetComponent<CardHandlers>();
        valueCntrllr = GameObject.Find("ValueController").GetComponent<ValueController>();

        turnCount = 1;

        WhoFirst();

        //cardHandler.FirstTableCard();
    }

    public void WhoFirst()
    {
        whoFirst = Random.Range(1, 10);

        //whoFirst = 2;

        if (whoFirst % 2 == 0)
        {
            algorithm2Turn = true;
            algorithm1Turn = false;

            if (GameController.bothPlay)
            {
                DetailPanelController.InstantiateTheText("Putaran ke-" + turnCount);
                DetailPanelController.InstantiateTheText("DnC Main Pertama");
                Debug.Log("Divide and Conquer Pertama");
            }

            else if (GameController.greedyPlay)
            {
                DetailPanelController.InstantiateTheText("Putaran ke-" + turnCount);
                DetailPanelController.InstantiateTheText("Greedy2 Main Pertama");
            }

            else if (GameController.dncPlay)
            {
                DetailPanelController.InstantiateTheText("Putaran ke-" + turnCount);
                DetailPanelController.InstantiateTheText("DnC2 Main Pertama");
            }
        }
        else
        {
            algorithm2Turn = false;
            algorithm1Turn = true;

            if (GameController.bothPlay)
            {
                DetailPanelController.InstantiateTheText("Putaran ke-" + turnCount);
                DetailPanelController.InstantiateTheText("Greedy Main Pertama");
                Debug.Log("Greedy Pertama");
            }

            if (GameController.greedyPlay)
            {
                DetailPanelController.InstantiateTheText("Putaran ke-" + turnCount);
                DetailPanelController.InstantiateTheText("Greedy1 Main Pertama");
                Debug.Log("Greedy Pertama");
            }

            if (GameController.dncPlay)
            {
                DetailPanelController.InstantiateTheText("Putaran ke-" + turnCount);
                DetailPanelController.InstantiateTheText("DnC1 Main Pertama");
                Debug.Log("Greedy Pertama");
            }
        }

        turnCount++;
    }

    public void ChangeTurn()
    {
        if (algorithm2Turn)
        {
            algorithm2Turn = false;
            algorithm1Turn = true;

            if (GameController.bothPlay)
            {
                DetailPanelController.InstantiateTheText("Top Value : " + valueCntrllr.valueTop);
                DetailPanelController.InstantiateTheText("Bot Value : " + valueCntrllr.valueBot);
                DetailPanelController.InstantiateTheText("DnC Selesai");
                DetailPanelController.InstantiateTheText("Putaran ke-" + turnCount);
                DetailPanelController.InstantiateTheText("Greedy Giliran Main");
            }

            else if (GameController.greedyPlay)
            {
                DetailPanelController.InstantiateTheText("Top Value : " + valueCntrllr.valueTop);
                DetailPanelController.InstantiateTheText("Bot Value : " + valueCntrllr.valueBot);
                DetailPanelController.InstantiateTheText("Greedy2 Selesai");
                DetailPanelController.InstantiateTheText("Putaran ke-" + turnCount);
                DetailPanelController.InstantiateTheText("Greedy1 Giliran Main");
            }

            else if (GameController.dncPlay)
            {
                DetailPanelController.InstantiateTheText("Top Value : " + valueCntrllr.valueTop);
                DetailPanelController.InstantiateTheText("Bot Value : " + valueCntrllr.valueBot);
                DetailPanelController.InstantiateTheText("DnC2 Selesai");
                DetailPanelController.InstantiateTheText("Putaran ke-" + turnCount);
                DetailPanelController.InstantiateTheText("DnC1 Giliran Main");
            }
        }

        else
        {
            algorithm2Turn = true;
            algorithm1Turn = false;

            if (GameController.bothPlay)
            {
                DetailPanelController.InstantiateTheText("Top Value : " + valueCntrllr.valueTop);
                DetailPanelController.InstantiateTheText("Bot Value : " + valueCntrllr.valueBot);
                DetailPanelController.InstantiateTheText("Greedy Selesai");
                DetailPanelController.InstantiateTheText("Putaran ke-" + turnCount);
                DetailPanelController.InstantiateTheText("DnC Giliran Main");
            }

            else if (GameController.greedyPlay)
            {
                DetailPanelController.InstantiateTheText("Top Value : " + valueCntrllr.valueTop);
                DetailPanelController.InstantiateTheText("Bot Value : " + valueCntrllr.valueBot);
                DetailPanelController.InstantiateTheText("Greedy1 Selesai");
                DetailPanelController.InstantiateTheText("Putaran ke-" + turnCount);
                DetailPanelController.InstantiateTheText("Greedy2 Giliran Main");
            }

            else if (GameController.dncPlay)
            {
                DetailPanelController.InstantiateTheText("Top Value : " + valueCntrllr.valueTop);
                DetailPanelController.InstantiateTheText("Bot Value : " + valueCntrllr.valueBot);
                DetailPanelController.InstantiateTheText("DnC1 Selesai");
                DetailPanelController.InstantiateTheText("Putaran ke-" + turnCount);
                DetailPanelController.InstantiateTheText("DnC2 Giliran Main");
            }
        }

        turnCount++;
    }
}
