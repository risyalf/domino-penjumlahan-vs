﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Kartu : MonoBehaviour
{
    public int topValue;
    public int botValue;

    int value1;
    int value2;

    CardChildTop topChild;
    CardChildBot botChild;

    //public string cardName;

    public int codeCard;

    /*bool enteringHitBoxTop;
    bool enteringHitBoxBottom;
    
    CardPosition theCardPosScript;
    CardHandler theCardHandlerScript;
    CursorFollow thisCursorFollow;

    LayerMask layerMask;

    */

    /*public void Start()
    {
        value1 = topValue;
        value2 = botValue;
                
        theCardPosScript = GameObject.Find("CardPosition").GetComponent<CardPosition>();

        theCardHandlerScript = GameObject.Find("CardHandler").GetComponent<CardHandler>();

        //thisSpriteRenderer.sortingOrder = 29;
                
        gameObject.SetActive(true);
        
        enteringHitBoxTop = false;
        enteringHitBoxBottom = false;

    }

    public void GetCursorFollowOff()
    {
        gameObject.GetComponent<CursorFollow>().enabled = false;
        gameObject.GetComponent<Collider2D>().enabled = false;
    }

    public void GetCursorFollowOn()
    {
        gameObject.GetComponent<CursorFollow>().enabled = true;
        gameObject.GetComponent<Collider2D>().enabled = true;
    }*/

    private void Start()
    {

    }

    public void GetValue()
    {
        Debug.Log("TopValue = " + topValue);
        Debug.Log("BotValue = " + botValue);
    }

    public void StartPositionChild()
    {
        topChild = GetComponentInChildren<CardChildTop>();
        botChild = GetComponentInChildren<CardChildBot>();

        topChild.transform.localPosition = new Vector3(0, 0, transform.localPosition.z);
        botChild.transform.localPosition = new Vector3(0, 0, transform.localPosition.z);
    }

    public void swapChild()
    {
        topChild = GetComponentInChildren<CardChildTop>();
        botChild = GetComponentInChildren<CardChildBot>();

        topChild.transform.localPosition = new Vector3(0, -600, transform.localPosition.z);
        botChild.transform.localPosition = new Vector3(0, 600, transform.localPosition.z);
    }

    public int GetTopValue()
    {
        return topValue;
    }

    public int GetBotValue()
    {
        return botValue;
    }

    /*public string GetName()
    {
        return cardName;         
    }*/

    public int GetCode()
    {
        return codeCard;
    }
    

    /*private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "TopCardPos")
        {
            Debug.Log("Hit Top");
            enteringHitBoxTop = true;
            enteringHitBoxBottom = false;
        }

        else if (collision.gameObject.name == "BottomCardPos")
        {
            Debug.Log("Hit Bot");
            enteringHitBoxTop = false;
            enteringHitBoxBottom = true;
        }

        else
        {
            enteringHitBoxTop = false;
            enteringHitBoxBottom = false;
        }
    }


    private void OnMouseUp()
    {
        if (enteringHitBoxTop == true)
        {
            Debug.Log("Mouse terdeteksi pada top hitbox");

            theCardPosScript.cardInThePlay.Add(theCardHandlerScript.deck[codeCard]);

            theCardPosScript.isTop = true;
            theCardPosScript.isBot = false;
        }

        else if (enteringHitBoxBottom == true)
        {
            Debug.Log("Mouse terdeteksi pada bot hitbox");

            theCardPosScript.cardInThePlay.Add(theCardHandlerScript.deck[codeCard]);
            
            theCardPosScript.isTop = false;
            theCardPosScript.isBot = true;

        }
    }*/
}