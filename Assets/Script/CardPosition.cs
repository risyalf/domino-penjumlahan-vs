﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPosition : MonoBehaviour
{
    public List<Kartu> dataBaseCard = new List<Kartu>();
    public List<Kartu> cardInThePlay = new List<Kartu>();

    //GameObject cardObject;

    public Kartu thisCard;
    //CursorFollow cursorFollowThisCard;

    ValueController theValue;

    CardHandler cardHandler;

    TurnController gameController;

    SpriteRenderer cardLayer;

    public int limitTopCount;
    public bool topCanScroll;

    public int limitBotCount;
    public bool botCanScroll;

    int databaseCardCode;

    int databaseCardCount;

    int whichCard;

    float zPos;

    string cardName;
    int codeCard;

    public bool isTop;
    public bool isBot;
    
    public Transform topPos;
    public GameObject topParent;
    int topCount;

    public Transform botPos;
    public GameObject botParent;
    int botCount;

    Transform tableScroll;

    public Transform resetPos;

    GameObject parentPos;

    //SpriteRenderer childSpriteRenderer;

    public bool canReplace;
    public bool signalToCard;

    int counter = 1;

    private void Start()
    {
        tableScroll = GameObject.Find("ScrollTableCard").GetComponent<Transform>();

        parentPos = GameObject.Find("ScrollTableCard");

        gameController = GameObject.Find("GameController").GetComponent<TurnController>();

        theValue = GameObject.Find("ValueController").GetComponent<ValueController>();

        cardHandler = GameObject.Find("CardHandler").GetComponent<CardHandler>();

        canReplace = false;

        whichCard = 0;

        databaseCardCount = 0;

        zPos = 0.1f;

        topCount = 1;
        botCount = 1;

        signalToCard = false;

        limitTopCount = 0;
        limitBotCount = 0;

        topCanScroll = false;
        botCanScroll = false;
    }

    private void Update()
    {
        topPos.position = new Vector3(topPos.position.x, tableScroll.position.y + (topCount * 6.1f), topPos.position.z);
        botPos.position = new Vector3(botPos.position.x, tableScroll.position.y - (botCount * 6.1f), botPos.position.z);

        if (cardInThePlay.Count > 0 && counter == 1)
        {
            dataBaseCard.Add(cardInThePlay[0]);

            /*thisCard = dataBaseCard[whichCard];
            cursorFollowThisCard = thisCard.GetComponent<CursorFollow>();

            thisCard.GetCursorFollowOff();*/

            //if (cardInThePlay[0].botValue == theValue.valueTop || cardInThePlay[0].topValue == theValue.valueBot || cardInThePlay[0].topValue == theValue.valueTop || cardInThePlay[0].botValue == theValue.valueBot)
            //{
            if (isTop == true)
            {
                if (cardInThePlay[0].botValue == theValue.valueTop)
                {
                    Debug.Log("Card Pos = Top Position");

                    //topPos.position = new Vector3(topPos.position.x, tableScroll.position.y + (topCount * 6), topPos.position.z);

                    thisCard = Instantiate(dataBaseCard[whichCard]) as Kartu;

                    signalToCard = true;

                    thisCard.transform.position = new Vector3(topPos.position.x, topPos.position.y, topPos.position.z - zPos);
                    
                    //thisCard.GetCursorFollowOff();

                    thisCard.transform.SetParent(parentPos.transform);

                    //topPos.position = new Vector3(topPos.position.x, (topPos.position.y + 6) - tableScroll.position.y, topPos.position.z);

                    theValue.valueTop = thisCard.topValue;

                    databaseCardCount++;
                    counter = 0;

                    /*for (int i = 0; i < cardHandler.drawedCard.Count; i++)
                    {
                        if (cardHandler.drawedCard[i].codeCard == thisCard.codeCard)
                        {
                            cardHandler.drawedCard.Remove(cardHandler.drawedCard[i]);
                        }
                    }*/

                    if (gameController.algorithm2Turn)
                    {
                        for (int i = 0; i < cardHandler.greedyCards.Count; i++)
                        {
                            if (cardHandler.greedyCards[i].codeCard == thisCard.codeCard)
                            {
                                cardHandler.greedyCards.Remove(cardHandler.greedyCards[i]);
                            }
                        }
                    }

                    else if (gameController.algorithm1Turn)
                    {
                        for (int i = 0; i < cardHandler.dncCards.Count; i++)
                        {
                            if (cardHandler.dncCards[i].codeCard == thisCard.codeCard)
                            {
                                cardHandler.dncCards.Remove(cardHandler.dncCards[i]);
                            }
                        }
                    }

                    topCount++;
                    gameController.ChangeTurn();
                    cardHandler.isResetCardHand = true;
                }

                else if (cardInThePlay[0].topValue == theValue.valueTop)
                {
                    Debug.Log("Card Pos = Top Position");

                    //topPos.position = new Vector3(topPos.position.x, tableScroll.position.y + (topCount * 6), topPos.position.z);

                    thisCard = Instantiate(dataBaseCard[whichCard]) as Kartu;

                    signalToCard = true;

                    thisCard.transform.position = new Vector3(topPos.position.x, topPos.position.y, topPos.position.z - zPos);
                    
                    //thisCard.GetCursorFollowOff();

                    thisCard.transform.SetParent(parentPos.transform);

                    //topPos.position = new Vector3(topPos.position.x, (topPos.position.y + 6) - tableScroll.position.y, topPos.position.z);

                    theValue.valueTop = thisCard.botValue;

                    databaseCardCount++;
                    counter = 0;

                    /*for (int i = 0; i < cardHandler.drawedCard.Count; i++)
                    {
                        if (cardHandler.drawedCard[i].codeCard == thisCard.codeCard)
                        {
                            cardHandler.drawedCard.Remove(cardHandler.drawedCard[i]);
                        }
                    }*/

                    if (gameController.algorithm2Turn)
                    {
                        for (int i = 0; i < cardHandler.greedyCards.Count; i++)
                        {
                            if (cardHandler.greedyCards[i].codeCard == thisCard.codeCard)
                            {
                                cardHandler.greedyCards.Remove(cardHandler.greedyCards[i]);
                            }
                        }
                    }

                    else if (gameController.algorithm1Turn)
                    {
                        for (int i = 0; i < cardHandler.dncCards.Count; i++)
                        {
                            if (cardHandler.dncCards[i].codeCard == thisCard.codeCard)
                            {
                                cardHandler.dncCards.Remove(cardHandler.dncCards[i]);
                            }
                        }
                    }

                    topCount++;
                    gameController.ChangeTurn();
                    cardHandler.isResetCardHand = true;
                    thisCard.swapChild();
                }

                topCanScroll = true;
                limitTopCount++;
            }

            else if (isBot == true)
            {
                if (cardInThePlay[0].topValue == theValue.valueBot)
                {
                    Debug.Log("Card Pos = Bot Position");

                    //botPos.position = new Vector3(botPos.position.x, tableScroll.position.y - (botCount * 6), botPos.position.z);

                    thisCard = Instantiate(dataBaseCard[whichCard]) as Kartu;

                    signalToCard = true;

                    thisCard.transform.position = new Vector3(botPos.position.x, botPos.position.y, botPos.position.z - zPos);
                    
                    //thisCard.GetCursorFollowOff();

                    thisCard.transform.SetParent(parentPos.transform);

                    //botPos.position = new Vector3(botPos.position.x, (botPos.position.y - 6) + tableScroll.position.y, botPos.position.z);

                    theValue.valueBot = cardInThePlay[0].botValue;

                    databaseCardCount++;
                    counter = 0;

                    /*for (int i = 0; i < cardHandler.drawedCard.Count; i++)
                    {
                        if (cardHandler.drawedCard[i].codeCard == thisCard.codeCard)
                        {
                            cardHandler.drawedCard.Remove(cardHandler.drawedCard[i]);
                        }
                    }*/

                    if (gameController.algorithm2Turn)
                    {
                        for (int i = 0; i < cardHandler.greedyCards.Count; i++)
                        {
                            if (cardHandler.greedyCards[i].codeCard == thisCard.codeCard)
                            {
                                cardHandler.greedyCards.Remove(cardHandler.greedyCards[i]);
                            }
                        }
                    }

                    else if(gameController.algorithm1Turn)
                    {
                        for (int i = 0; i < cardHandler.dncCards.Count; i++)
                        {
                            if (cardHandler.dncCards[i].codeCard == thisCard.codeCard)
                            {
                                cardHandler.dncCards.Remove(cardHandler.dncCards[i]);
                            }
                        }
                    }

                    botCount++;
                    gameController.ChangeTurn();
                    cardHandler.isResetCardHand = true;
                }

                else if(cardInThePlay[0].botValue == theValue.valueBot)
                {
                    Debug.Log("Card Pos = Bot Position");

                    //botPos.position = new Vector3(botPos.position.x, tableScroll.position.y - (botCount * 6), botPos.position.z);

                    thisCard = Instantiate(dataBaseCard[whichCard]) as Kartu;

                    signalToCard = true;

                    thisCard.transform.position = new Vector3(botPos.position.x, botPos.position.y, botPos.position.z - zPos);
                    
                    //thisCard.GetCursorFollowOff();

                    thisCard.transform.SetParent(parentPos.transform);

                    //botPos.position = new Vector3(botPos.position.x, botPos.position.y - 6, botPos.position.z);

                    theValue.valueBot = cardInThePlay[0].topValue;

                    databaseCardCount++;
                    counter = 0;

                    /*for (int i = 0; i < cardHandler.drawedCard.Count; i++)
                    {
                        if (cardHandler.drawedCard[i].codeCard == thisCard.codeCard)
                        {
                            cardHandler.drawedCard.Remove(cardHandler.drawedCard[i]);
                        }
                    }*/

                    if (gameController.algorithm2Turn)
                    {
                        for (int i = 0; i < cardHandler.greedyCards.Count; i++)
                        {
                            if (cardHandler.greedyCards[i].codeCard == thisCard.codeCard)
                            {
                                cardHandler.greedyCards.Remove(cardHandler.greedyCards[i]);
                            }
                        }
                    }

                    else if (gameController.algorithm1Turn)
                    {
                        for (int i = 0; i < cardHandler.dncCards.Count; i++)
                        {
                            if (cardHandler.dncCards[i].codeCard == thisCard.codeCard)
                            {
                                cardHandler.dncCards.Remove(cardHandler.dncCards[i]);
                            }
                        }
                    }

                    botCount++;
                    gameController.ChangeTurn();
                    cardHandler.isResetCardHand = true;
                    thisCard.swapChild();
                }

                botCanScroll = true;
                limitBotCount++;
            }
            //}
            else
            {
                signalToCard = false;
                dataBaseCard.Remove(dataBaseCard[databaseCardCount]);

                if (databaseCardCount > 0)
                {
                    databaseCardCount--;
                }

                counter = 0;
            }

            zPos++;
            whichCard++;

            cardInThePlay.Remove(cardInThePlay[0]);
            counter = 1;
        }
        else { return; }        
    }

}