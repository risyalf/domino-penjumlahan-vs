﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailPanelController : MonoBehaviour
{
    public List<GameObject> setDetailPanelBox = new List<GameObject>();
    
    static int ctr;
    static List<GameObject> detailPanelBox = new List<GameObject>();
    static GameObject areaText;

    private void Awake()
    {
        detailPanelBox = setDetailPanelBox;
    }

    private void Start()
    {
        areaText = GameObject.Find("AreaText");
        ctr = 0;
    }

    public static void InstantiateTheText(string teks) 
    {
        GameObject theText = Instantiate(detailPanelBox[0]);
        theText.transform.SetParent(areaText.transform);

        if (ctr < 18)
        {
            theText.transform.localPosition = new Vector3(0, 212.5f - (25 * ctr), 0);
            theText.transform.localScale = new Vector3(1, 1, 1);

            Text kata = theText.GetComponentInChildren<Text>();
            kata.text = teks;
        }

        ctr++;

        if (ctr > 18)
        {
            RectTransform areaTextRT = areaText.GetComponent<RectTransform>();
            areaTextRT.sizeDelta = new Vector2(244, 455 + (25 * (ctr - 18)));

            theText.transform.localPosition = new Vector3(0, -225 - (12.5f * (ctr - 19)), 0);
            theText.transform.localScale = new Vector3(1, 1, 1);

            areaTextRT.localPosition = new Vector3(areaTextRT.localPosition.x, areaTextRT.localPosition.y + 12.5f, areaTextRT.localPosition.z);

            Text kata = theText.GetComponentInChildren<Text>();
            kata.text = teks;
        }
    }
}
