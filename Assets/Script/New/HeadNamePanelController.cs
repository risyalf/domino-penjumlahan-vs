﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeadNamePanelController : MonoBehaviour
{
    Text algorithm1Text;
    Text algorithm2Text;

    void Start()
    {
        algorithm1Text = GameObject.Find("GreedyText").GetComponent<Text>();
        algorithm2Text = GameObject.Find("DnCText").GetComponent<Text>();

        ChangeText();
    }

    void ChangeText() 
    {
        if (GameController.bothPlay) 
        {
            algorithm1Text.text = "Greedy";
            algorithm2Text.text = "Divide and Conquer";
        }

        else if (GameController.greedyPlay)
        {
            algorithm1Text.text = "Greedy 1";
            algorithm2Text.text = "Greedy 2";
        }

        else if (GameController.dncPlay)
        {
            algorithm1Text.text = "Divide and Conquer 1";
            algorithm2Text.text = "Divide and Conquer 2";
        }
    }
}
