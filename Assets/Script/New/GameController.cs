﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    AIController controllerAI;
    AIHandler aiHndlr;
    CardHandlers cardHndlr;
    GameController gameCntrllr;
    TurnController turnContrller;

    int algorithm1Count;
    int algorithm2Count;
    float waitingTIme;

    bool endGame;
    public bool plsDraw;

    public static bool bothPlay;
    public static bool greedyPlay;
    public static bool dncPlay;

    void Awake()
    {
        aiHndlr = GameObject.Find("AIController").GetComponent<AIHandler>();
        controllerAI = GameObject.Find("AIController").GetComponent<AIController>();
        cardHndlr = GameObject.Find("CardHandler").GetComponent<CardHandlers>();
        gameCntrllr = GameObject.Find("GameController").GetComponent<GameController>();
        turnContrller = GameObject.Find("TurnController").GetComponent<TurnController>();

        waitingTIme = 0.5f;
        algorithm1Count = 0;
        algorithm2Count = 0;

        endGame = false;
        plsDraw = false;
    }

    private void Start()
    {
        StartCoroutine(GameHandler());
    }

    private void Update()
    {
        if (cardHndlr.algorithm1Card.Count == 0 && endGame == false)
        {
            announceTheWinnerIsAlgorithm1();
        }
        if (cardHndlr.algorithm2Card.Count == 0 && endGame == false)
        {
            announceTheWinnerIsAlgorithm2();
        }
    }

    IEnumerator GameHandler()
    {
        while (true)
        {
            yield return new WaitForSeconds(waitingTIme);

            print("SortingKartu");
            controllerAI.AISortingController();

            yield return new WaitForSeconds(waitingTIme);

            controllerAI.AIActionController();

            yield return new WaitForSeconds(waitingTIme);

            if (cardHndlr.drawableCard.Count > 0)
            {
                if (plsDraw == true)
                {
                    cardHndlr.DrawCard();

                    print("Draw");

                    DetailPanelController.InstantiateTheText("Sisa Kartu Di Deck : " + cardHndlr.drawableCard.Count);
                    print("Sisa Kartu " + cardHndlr.drawableCard.Count);

                    yield return new WaitForSeconds(waitingTIme);

                    print("Sorting Kartu");
                    controllerAI.AISortingController();

                    yield return new WaitForSeconds(waitingTIme);

                    controllerAI.AIActionController();

                    plsDraw = false;

                    yield return new WaitForSeconds(waitingTIme);
                }
            }

            else if (cardHndlr.drawableCard.Count == 0 && plsDraw == true)
            {
                CountingEndGame();

                StopAllCoroutines();

                yield break;
            }

            turnContrller.ChangeTurn();
        }
    }

    int EndCountAlgorithm1()
    {
        int tmprCount;

        for (int i = 0; i < cardHndlr.algorithm1Card.Count; i++)
        {
            tmprCount = cardHndlr.algorithm1Card[i].topValue + cardHndlr.algorithm1Card[i].botValue;

            algorithm1Count = algorithm1Count + tmprCount;
        }

        return algorithm1Count;
    }

    int EndCountAlgorithm2()
    {
        int tmprCount;

        for (int i = 0; i < cardHndlr.algorithm2Card.Count; i++)
        {
            tmprCount = cardHndlr.algorithm2Card[i].topValue + cardHndlr.algorithm2Card[i].botValue;

            algorithm2Count = algorithm2Count + tmprCount;
        }

        return algorithm2Count;
    }

    public void CountingEndGame()
    {
        EndCountAlgorithm1();
        EndCountAlgorithm2();

        if (bothPlay)
        {
            DetailPanelController.InstantiateTheText("Greedy Draw Sebanyak : " + cardHndlr.algorithm1DrawCnt);
            DetailPanelController.InstantiateTheText("Greedy Lewati Putaran Sebanyak : " + aiHndlr.algorithm1PasCnt);
            DetailPanelController.InstantiateTheText("Greedy Sisa Kartu : " + cardHndlr.algorithm1Card.Count);
            DetailPanelController.InstantiateTheText("Greedy Nilai Sisa Kartu : " + algorithm1Count);
            print("Nilai Greedy adalah " + algorithm1Count);

            DetailPanelController.InstantiateTheText("DnC Draw Sebanyak : " + cardHndlr.algorithm2DrawCnt);
            DetailPanelController.InstantiateTheText("DnC Lewati Putaran Sebanyak : " + aiHndlr.algorithm2PasCnt);
            DetailPanelController.InstantiateTheText("DnC Sisa Kartu : " + cardHndlr.algorithm2Card.Count);
            DetailPanelController.InstantiateTheText("DnC Nilai Sisa Kartu : " + algorithm2Count);
            print("Nilai DnC adalah " + algorithm2Count);

            if (algorithm1Count < algorithm2Count)
            {
                DetailPanelController.InstantiateTheText("Greedy Menang");
                print("Pemenangnya Greedy");
            }

            else if (algorithm2Count < algorithm1Count)
            {
                DetailPanelController.InstantiateTheText("DnC Menang");
                print("Pemenangnya dnc");
            }
        }

        else if (greedyPlay)
        {
            DetailPanelController.InstantiateTheText("Greedy1 Draw Sebanyak : " + cardHndlr.algorithm1DrawCnt);
            DetailPanelController.InstantiateTheText("Greedy1 Lewati Putaran Sebanyak : " + aiHndlr.algorithm1PasCnt);
            DetailPanelController.InstantiateTheText("Greedy1 Sisa Kartu : " + cardHndlr.algorithm1Card.Count);
            DetailPanelController.InstantiateTheText("Greedy1 Nilai Sisa Kartu : " + algorithm1Count);

            DetailPanelController.InstantiateTheText("Greedy2 Draw Sebanyak : " + cardHndlr.algorithm2DrawCnt);
            DetailPanelController.InstantiateTheText("Greedy2 Lewati Putaran Sebanyak : " + aiHndlr.algorithm2PasCnt);
            DetailPanelController.InstantiateTheText("Greedy2 Sisa Kartu : " + cardHndlr.algorithm2Card.Count);
            DetailPanelController.InstantiateTheText("Greedy2 Nilai Sisa Kartu : " + algorithm2Count);

            if (algorithm1Count < algorithm2Count)
            {
                DetailPanelController.InstantiateTheText("Greedy1 Menang");
            }

            else if (algorithm2Count < algorithm1Count)
            {
                DetailPanelController.InstantiateTheText("Greedy2 Menang");
            }
        }

        else if (dncPlay)
        {
            DetailPanelController.InstantiateTheText("DnC1 Draw Sebanyak : " + cardHndlr.algorithm1DrawCnt);
            DetailPanelController.InstantiateTheText("DnC1 Lewati Putaran Sebanyak : " + aiHndlr.algorithm1PasCnt);
            DetailPanelController.InstantiateTheText("DnC1 Sisa Kartu : " + cardHndlr.algorithm1Card.Count);
            DetailPanelController.InstantiateTheText("DnC1 Nilai Sisa Kartu : " + algorithm1Count);

            DetailPanelController.InstantiateTheText("DnC2 Draw Sebanyak : " + cardHndlr.algorithm2DrawCnt);
            DetailPanelController.InstantiateTheText("DnC2 Lewati Putaran Sebanyak : " + aiHndlr.algorithm2PasCnt);
            DetailPanelController.InstantiateTheText("DnC2 Sisa Kartu : " + cardHndlr.algorithm2Card.Count);
            DetailPanelController.InstantiateTheText("DnC2 Nilai Sisa Kartu : " + algorithm2Count);

            if (algorithm1Count < algorithm2Count)
            {
                DetailPanelController.InstantiateTheText("DnC1 Menang");
            }

            else if (algorithm2Count < algorithm1Count)
            {
                DetailPanelController.InstantiateTheText("DnC2 Menang");
            }
        }
    }

    void announceTheWinnerIsAlgorithm1()
    {
        if (bothPlay)
        {
            DetailPanelController.InstantiateTheText("Greedy Menang");

            DetailPanelController.InstantiateTheText("Greedy Draw Sebanyak : " + cardHndlr.algorithm1DrawCnt);
            DetailPanelController.InstantiateTheText("DnC Draw Sebanyak : " + cardHndlr.algorithm2DrawCnt);

            DetailPanelController.InstantiateTheText("Greedy Lewati Putaran Sebanyak : " + aiHndlr.algorithm1PasCnt);
            DetailPanelController.InstantiateTheText("DnC Lewati Putaran Sebanyak : " + aiHndlr.algorithm2PasCnt);

            DetailPanelController.InstantiateTheText("DnC Sisa Kartu : " + cardHndlr.algorithm2Card.Count);
            DetailPanelController.InstantiateTheText("DnC Nilai Sisa Kartu : " + EndCountAlgorithm2());
            Debug.Log("Pemenangnya adalah Greedy");
        }

        else if (greedyPlay)
        {
            DetailPanelController.InstantiateTheText("Greedy1 Menang");

            DetailPanelController.InstantiateTheText("Greedy1 Draw Sebanyak : " + cardHndlr.algorithm1DrawCnt);
            DetailPanelController.InstantiateTheText("Greedy2 Draw Sebanyak : " + cardHndlr.algorithm2DrawCnt);

            DetailPanelController.InstantiateTheText("Greedy1 Lewati Putaran Sebanyak : " + aiHndlr.algorithm1PasCnt);
            DetailPanelController.InstantiateTheText("Greedy2 Lewati Putaran Sebanyak : " + aiHndlr.algorithm2PasCnt);

            DetailPanelController.InstantiateTheText("Greedy2 Sisa Kartu : " + cardHndlr.algorithm2Card.Count);
            DetailPanelController.InstantiateTheText("Greedy2 Nilai Sisa Kartu : " + EndCountAlgorithm2());
        }

        else if (dncPlay)
        {
            DetailPanelController.InstantiateTheText("DnC1 Menang");

            DetailPanelController.InstantiateTheText("DnC1 Draw Sebanyak : " + cardHndlr.algorithm1DrawCnt);
            DetailPanelController.InstantiateTheText("DnC2 Draw Sebanyak : " + cardHndlr.algorithm2DrawCnt);

            DetailPanelController.InstantiateTheText("DnC1 Lewati Putaran Sebanyak : " + aiHndlr.algorithm1PasCnt);
            DetailPanelController.InstantiateTheText("DnC2 Lewati Putaran Sebanyak : " + aiHndlr.algorithm2PasCnt);

            DetailPanelController.InstantiateTheText("DnC2 Sisa Kartu : " + cardHndlr.algorithm2Card.Count);
            DetailPanelController.InstantiateTheText("DnC2 Nilai Sisa Kartu : " + EndCountAlgorithm2());
        }

        endGame = true;

        StopAllCoroutines();
    }

    void announceTheWinnerIsAlgorithm2()
    {
        if (bothPlay)
        {
            DetailPanelController.InstantiateTheText("DnC Menang");

            DetailPanelController.InstantiateTheText("Greedy Draw Sebanyak : " + cardHndlr.algorithm1DrawCnt);
            DetailPanelController.InstantiateTheText("DnC Draw Sebanyak : " + cardHndlr.algorithm2DrawCnt);

            DetailPanelController.InstantiateTheText("Greedy Lewati Putaran Sebanyak : " + aiHndlr.algorithm1PasCnt);
            DetailPanelController.InstantiateTheText("DnC Lewati Putaran Sebanyak : " + aiHndlr.algorithm2PasCnt);

            DetailPanelController.InstantiateTheText("Greedy Sisa Kartu : " + cardHndlr.algorithm1Card.Count);
            DetailPanelController.InstantiateTheText("Greedy Nilai Sisa Kartu : " + EndCountAlgorithm1());
            Debug.Log("Pemenangnya adalah DnC");
        }

        else if (greedyPlay)
        {
            DetailPanelController.InstantiateTheText("Greedy2 Menang");

            DetailPanelController.InstantiateTheText("Greedy1 Draw Sebanyak : " + cardHndlr.algorithm1DrawCnt);
            DetailPanelController.InstantiateTheText("Greedy2 Draw Sebanyak : " + cardHndlr.algorithm2DrawCnt);

            DetailPanelController.InstantiateTheText("Greedy1 Lewati Putaran Sebanyak : " + aiHndlr.algorithm1PasCnt);
            DetailPanelController.InstantiateTheText("Greedy2 Lewati Putaran Sebanyak : " + aiHndlr.algorithm2PasCnt);

            DetailPanelController.InstantiateTheText("Greedy1 Sisa Kartu : " + cardHndlr.algorithm1Card.Count);
            DetailPanelController.InstantiateTheText("Greedy1 Nilai Sisa Kartu : " + EndCountAlgorithm1());
        }

        else if (dncPlay)
        {
            DetailPanelController.InstantiateTheText("DnC2 Menang");

            DetailPanelController.InstantiateTheText("DnC1 Draw Sebanyak : " + cardHndlr.algorithm1DrawCnt);
            DetailPanelController.InstantiateTheText("DnC2 Draw Sebanyak : " + cardHndlr.algorithm2DrawCnt);

            DetailPanelController.InstantiateTheText("DnC1 Lewati Putaran Sebanyak : " + aiHndlr.algorithm1PasCnt);
            DetailPanelController.InstantiateTheText("DnC2 Lewati Putaran Sebanyak : " + aiHndlr.algorithm2PasCnt);

            DetailPanelController.InstantiateTheText("DnC1 Sisa Kartu : " + cardHndlr.algorithm1Card.Count);
            DetailPanelController.InstantiateTheText("DnC1 Nilai Sisa Kartu : " + EndCountAlgorithm1());
        }

        endGame = true;

        StopAllCoroutines();
    }
}
