﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardHandlers : MonoBehaviour
{
    Kartu chosenCard;
    CardTableHandler crdTblHndlr;
    TurnController turnCntrllr;
    ValueController vluCntrllr;

    public List<Kartu> deck = new List<Kartu>();
    public List<Kartu> drawableCard = new List<Kartu>();
    public List<Kartu> algorithm1Card = new List<Kartu>();
    public List<Kartu> algorithm2Card = new List<Kartu>();

    static RectTransform cardAreaAlgorithm1;
    static RectTransform cardAreaAlgorithm2;

    float cardAreaDistance;
    float cardDistance;

    public int rndmNbr;
    public int algorithm1DrawCnt;
    public int algorithm2DrawCnt;

    private void Awake()
    {
        moveCardFromDeckToDrawableCard();
    }

    // Start is called before the first frame update
    void Start()
    {
        cardAreaAlgorithm1 = GameObject.Find("CardAreaGreedy").GetComponent<RectTransform>();
        cardAreaAlgorithm2 = GameObject.Find("CardAreaDnC").GetComponent<RectTransform>();
        crdTblHndlr = GameObject.Find("CardTableHandler").GetComponent<CardTableHandler>();
        vluCntrllr = GameObject.Find("ValueController").GetComponent<ValueController>();
        turnCntrllr = GameObject.Find("TurnController").GetComponent<TurnController>();

        cardAreaDistance = 60;
        cardDistance = 30;

        algorithm2DrawCnt = 0;
        algorithm1DrawCnt = 0;

        AddCardOnTable();
        addFirstCardsToBothAI();
    }
    
    public int Suffle()
    {
        return rndmNbr = Random.Range(0, drawableCard.Count);
    }

    public void AddCard() 
    {
        if (turnCntrllr.algorithm1Turn)
        {
            AddCardToAlgorithm1();
        }

        if (turnCntrllr.algorithm2Turn) 
        {
            AddCardToAlgorithm2();
        }        
    }
    public void DrawCard()
    {
        if (drawableCard.Count > 0)
        {
            if (turnCntrllr.algorithm1Turn)
            {
                AddCardToAlgorithm1();

                if (GameController.bothPlay)
                {
                    DetailPanelController.InstantiateTheText("Greedy Drawing Card");
                }

                else if(GameController.greedyPlay)
                {
                    DetailPanelController.InstantiateTheText("Greedy1 Drawing Card");
                }

                else if (GameController.dncPlay)
                {
                    DetailPanelController.InstantiateTheText("DnC1 Drawing Card");
                }

                algorithm1DrawCnt++;
            }

            else if (turnCntrllr.algorithm2Turn)
            {
                AddCardToAlgorithm2();

                if (GameController.bothPlay)
                {
                    DetailPanelController.InstantiateTheText("DnC Drawing Card");
                }

                else if (GameController.greedyPlay)
                {
                    DetailPanelController.InstantiateTheText("Greedy2 Drawing Card");
                }

                else if (GameController.dncPlay)
                {
                    DetailPanelController.InstantiateTheText("DnC2 Drawing Card");
                }

                algorithm2DrawCnt++;
            }
        }


        else
        {
            print("Deck telah habis");
        }
    }

    public void AddCardOnTable() 
    {
        Kartu firstCard;
        RectTransform cardTbl = GameObject.Find("CardTable").GetComponent<RectTransform>();

        Suffle();

        firstCard = Instantiate(drawableCard[rndmNbr], cardTbl.localPosition, cardTbl.localRotation) ;

        firstCard.transform.SetParent(cardTbl.transform, false);

        firstCard.name = drawableCard[rndmNbr].name;

        RectTransform cardRT;
        cardRT = firstCard.GetComponent<RectTransform>();

        cardRT.localPosition = new Vector3(0, 0, 0);
        cardRT.localScale = new Vector3(0.077f, 0.077f, 0);

        crdTblHndlr.cardOnTable.Add(drawableCard[rndmNbr]);

        vluCntrllr.valueTop = firstCard.topValue;
        vluCntrllr.valueBot = firstCard.botValue;

        vluCntrllr.BeginningValueSet();

        drawableCard.Remove(drawableCard[rndmNbr]);        
    }

    void AddCardToAlgorithm1()
    {
        Suffle();

        algorithm1Card.Add(drawableCard[rndmNbr]);

        cardAreaAlgorithm1.sizeDelta = new Vector2(cardAreaDistance * algorithm1Card.Count, 100);

        chosenCard = Instantiate(drawableCard[rndmNbr], cardAreaAlgorithm1.position, cardAreaAlgorithm1.rotation);        

        chosenCard.transform.SetParent(cardAreaAlgorithm1.transform, true);

        chosenCard.name = drawableCard[rndmNbr].name;

        RectTransform cardRT;
        cardRT = chosenCard.GetComponent<RectTransform>();

        cardRT.localPosition = new Vector3(cardDistance * (algorithm1Card.Count - 1) , 0, 0);
        cardRT.localScale = new Vector3(0.077f, 0.077f, 0);

        chosenCard.StartPositionChild();

        drawableCard.Remove(drawableCard[rndmNbr]);
    }

    void AddCardToAlgorithm2()
    {
        Suffle();

        algorithm2Card.Add(drawableCard[rndmNbr]);

        cardAreaAlgorithm2.sizeDelta = new Vector2(cardAreaDistance * algorithm2Card.Count, 100);

        chosenCard = Instantiate(drawableCard[rndmNbr], cardAreaAlgorithm2.localPosition, cardAreaAlgorithm2.rotation) ;

        chosenCard.transform.SetParent(cardAreaAlgorithm2.transform, false);

        chosenCard.name = drawableCard[rndmNbr].name;

        RectTransform cardRT;
        cardRT = chosenCard.GetComponent<RectTransform>();

        cardRT.localPosition = new Vector3(cardDistance * (algorithm2Card.Count - 1), 0, 0);
        cardRT.localScale = new Vector3(0.077f, 0.077f, 0);

        chosenCard.StartPositionChild();

        drawableCard.Remove(drawableCard[rndmNbr]);
    }

    void moveCardFromDeckToDrawableCard()
    {
        for (int i = 0; i < deck.Count; i++)
        {
            drawableCard.Add(deck[i]);
        }
    }

    void addFirstCardsToBothAI() 
    {
        for(int i = 0; i < 7; i++) 
        {
            AddCardToAlgorithm1();
            AddCardToAlgorithm2();
        }
    }

    public void ResetCardHand()
    {
        if (turnCntrllr.algorithm1Turn)
        {
            SortingOrderGreedy(algorithm1Card);
        }

        if (turnCntrllr.algorithm2Turn)
        {
            SortingOrderDnC(algorithm2Card);
        }
    }
    

    public void SortingOrderGreedy(List<Kartu> algorithm1Cards) 
    {
        foreach(Transform child in cardAreaAlgorithm1.transform) 
        {
            Destroy(child.gameObject);
        }
        
        for (int i = 0; i < algorithm1Cards.Count; i++)
        {
            cardAreaAlgorithm1.sizeDelta = new Vector2(60 + (cardAreaDistance * i), 100);

            Kartu instCard;

            instCard = Instantiate(algorithm1Cards[i], cardAreaAlgorithm1.position, cardAreaAlgorithm1.rotation);

            instCard.transform.SetParent(cardAreaAlgorithm1.transform, true);

            instCard.name = algorithm1Cards[i].name;

            RectTransform cardRT;
            cardRT = instCard.GetComponent<RectTransform>();

            cardRT.localPosition = new Vector3(cardDistance * ((i + 1) - 1), 0, 0);
            cardRT.localScale = new Vector3(0.077f, 0.077f, 0);
        }
    }

    public void SortingOrderDnC(List<Kartu> algorithm2Cards)
    {
        foreach (Transform child in cardAreaAlgorithm2.transform)
        {
            Destroy(child.gameObject);
        }

        for (int i = 0; i < algorithm2Cards.Count; i++)
        {
            cardAreaAlgorithm2.sizeDelta = new Vector2(60 + (cardAreaDistance * i), 100);

            Kartu instCard;

            instCard = Instantiate(algorithm2Cards[i], cardAreaAlgorithm2.position, cardAreaAlgorithm2.rotation);

            instCard.transform.SetParent(cardAreaAlgorithm2.transform, true);

            instCard.name = algorithm2Cards[i].name;

            RectTransform cardRT;
            cardRT = instCard.GetComponent<RectTransform>();

            cardRT.localPosition = new Vector3(cardDistance * ((i + 1) - 1), 0, 0);
            cardRT.localScale = new Vector3(0.077f, 0.077f, 0);
        }
    }
}
