﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CardMenuController : MonoBehaviour
{
    CardHandlers crdHndlr;
    CardTableHandler crdTblHndlr;

    Image pauseBtn;

    float timeScale;

    RectTransform cardTableRT;

    private void Start()
    {
        crdHndlr = GameObject.Find("CardHandler").GetComponent<CardHandlers>();
        crdTblHndlr = GameObject.Find("CardTableHandler").GetComponent<CardTableHandler>();

        cardTableRT = GameObject.Find("CardTable").GetComponent<RectTransform>();

        pauseBtn = GameObject.Find("PauseButton").GetComponent<Image>();

        pauseBtn.color = Color.white;

        timeScale = 1;
    }

    public void Pause()
    {
        pauseBtn.color = Color.gray;

        timeScale = Time.timeScale;

        Time.timeScale = 0;

        if (timeScale == 0) 
        {
            timeScale = 1;
        }
    }

    public void Resume()
    {
        pauseBtn.color = Color.white;

        Time.timeScale = timeScale;
    }

    public void ToTop() 
    {
        cardTableRT.localPosition = new Vector3(0, 0, 0);
        cardTableRT.localPosition = new Vector3(0, -75 * crdTblHndlr.topCardOnTable.Count, 0);
    }

    public void ToBot()
    {
        cardTableRT.localPosition = new Vector3(0, 0, 0);
        cardTableRT.localPosition = new Vector3(0, 75 * crdTblHndlr.botCardOnTable.Count, 0);
    }

    public void Faster() 
    {
        if (Time.timeScale > 0)
        {
            Time.timeScale = Time.timeScale + 0.5f;

            if (Time.timeScale > 2f)
            {
                Time.timeScale = 2f;
            }
        }

        print("Time Scale : " + Time.timeScale);
    }

    public void SlowDown()
    {
        if (Time.timeScale > 0)
        {
            Time.timeScale = Time.timeScale - 0.5f;

            if (Time.timeScale < 0.5f)
            {
                Time.timeScale = 0.5f;
            }
        }

        print("Time Scale : "+Time.timeScale);
    }

    public void Restart() 
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 0;
    }

    public void Quit()
    {
        GameController.bothPlay = false;
        GameController.greedyPlay = false;
        GameController.dncPlay = false;

        SceneManager.LoadScene(0);
    }
}
