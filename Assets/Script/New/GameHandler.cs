﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameHandler : MonoBehaviour
{
    private void Awake()
    {
        GameController.bothPlay = false;
        GameController.greedyPlay = false;
        GameController.dncPlay = false;
    }

    public void GreedyVSDnc() 
    {
        GameController.bothPlay = true;
        SceneManager.LoadScene(1);
        Time.timeScale = 0;
    }

    public void GreedyVSGreedy()
    {
        GameController.greedyPlay = true;
        SceneManager.LoadScene(1);
        Time.timeScale = 0;
    }

    public void DnCVSDnC()
    {
        GameController.dncPlay = true;
        SceneManager.LoadScene(1);
        Time.timeScale = 0;
    }
}
