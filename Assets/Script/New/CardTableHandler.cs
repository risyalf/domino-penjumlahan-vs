﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardTableHandler : MonoBehaviour
{
    CardHandlers crdHndlr;
    TurnController turnCntrllr;
    ValueController valueCntrllr;

    public List<Kartu> cardOnTable = new List<Kartu>();
    public List<Kartu> topCardOnTable = new List<Kartu>();
    public List<Kartu> botCardOnTable = new List<Kartu>();

    //Transform cardTable;

    int topCtr;
    int botCtr;

    void Start()
    {
        crdHndlr = GameObject.Find("CardHandler").GetComponent<CardHandlers>();
        turnCntrllr = GameObject.Find("TurnController").GetComponent<TurnController>();
        valueCntrllr = GameObject.Find("ValueController").GetComponent<ValueController>();

        //cardTable = GameObject.Find("CardTable").GetComponent<Transform>();

        topCtr = 0;
        botCtr = 0;
    }

    public void addCardToTopList(Kartu theCard) 
    {
        topCardOnTable.Add(theCard);
    }

    public void addCardToBotList(Kartu theCard)
    {
        botCardOnTable.Add(theCard);
    }

    public void InstantiateTopCard(List<Kartu> theCard, Transform tableCard) 
    {
        Kartu thisCard;

        //thisCard = Instantiate(theCard[topCtr], cardTable.localPosition, cardTable.localRotation);

        thisCard = Instantiate(theCard[topCtr], tableCard.localPosition, tableCard.localRotation); ;

        thisCard.transform.SetParent(tableCard);
        
        RectTransform rtCard;
        rtCard = thisCard.GetComponent<RectTransform>();

        rtCard.localPosition = new Vector3(0, 50 + (50 * topCtr), 0);
        rtCard.localScale = new Vector3(0.077f, 0.077f, 0);

        topCtr++;
    }

    public void InstantiateTopCardSwap(List<Kartu> theCard, Transform tableCard)
    {
        Kartu thisCard;

        //thisCard = Instantiate(theCard[topCtr], cardTable.localPosition, cardTable.localRotation);

        thisCard = Instantiate(theCard[topCtr], tableCard.localPosition, tableCard.localRotation); ;
        
        thisCard.transform.SetParent(tableCard);

        RectTransform rtCard;
        rtCard = thisCard.GetComponent<RectTransform>();

        rtCard.localPosition = new Vector3(0, 50 + (50 * topCtr), 0);
        rtCard.localScale = new Vector3(0.077f, 0.077f, 0);

        thisCard.swapChild();

        topCtr++;
    }

    public void InstantiateBotCard(List<Kartu> theCard, Transform tableCard)
    {
        Kartu thisCard;

        thisCard = Instantiate(theCard[botCtr], tableCard.localPosition, tableCard.localRotation);

        thisCard.transform.SetParent(tableCard);

        RectTransform rtCard;
        rtCard = thisCard.GetComponent<RectTransform>();

        rtCard.localPosition = new Vector3(0, -50 - (50 * botCtr), 0);
        rtCard.localScale = new Vector3(0.077f, 0.077f, 0);

        botCtr++;
    }

    public void InstantiateBotCardSwap(List<Kartu> theCard, Transform tableCard)
    {
        Kartu thisCard;

        thisCard = Instantiate(theCard[botCtr], tableCard.localPosition, tableCard.localRotation);

        thisCard.transform.SetParent(tableCard);

        RectTransform rtCard;
        rtCard = thisCard.GetComponent<RectTransform>();

        rtCard.localPosition = new Vector3(0, -50 - (50 * botCtr), 0);
        rtCard.localScale = new Vector3(0.077f, 0.077f, 0);

        thisCard.swapChild();

        botCtr++;
    }
}
