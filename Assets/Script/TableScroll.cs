﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableScroll : MonoBehaviour
{
    private bool selected;
    float cursorPos_x, cursorPos_y;
    bool countx, county;
    // Start is called before the first frame update
    void Start()
    {
        countx = false;
        county = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (selected == true)
        {
            Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector2(0, (cursorPos.y + getClickDistance_y(cursorPos.y)));
            Debug.Log(Input.mousePosition + " " + transform.position);
        }

        if (Input.GetMouseButtonUp(0))
        {
            countx = false;
            county = false;
            selected = false;
        }
    }

    float getClickDistance_x(float x)
    {

        if (countx == false)
        {
            cursorPos_x = transform.position.x - x;
            countx = true;
        }

        return cursorPos_x;
    }

    float getClickDistance_y(float y)
    {

        if (county == false)
        {
            cursorPos_y = transform.position.y - y;
            county = true;
        }

        return cursorPos_y;
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            selected = true;
        }
    }
}
