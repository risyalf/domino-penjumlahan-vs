﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValueController : MonoBehaviour
{
    public int valueTop;
    public int valueBot;
    Text topValueTxt;
    Text botValueTxt;

    private void Start()
    {
        topValueTxt = GameObject.Find("TopValueText").GetComponent<Text>();
        botValueTxt = GameObject.Find("BotValueText").GetComponent<Text>();
    }

    public void BeginningValueSet()
    {
        topValueTxt.text = valueTop.ToString();
        botValueTxt.text = valueBot.ToString();
    }

    public void ChangeTopValue(int topValue)
    {
        valueTop = topValue;

        topValueTxt.text = valueTop.ToString();
    }

    public void ChangeBotValue(int botValue)
    {
        valueBot = botValue;

        botValueTxt.text = valueBot.ToString();
    }
}
