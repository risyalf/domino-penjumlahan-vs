﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIHandler : MonoBehaviour
{
    CardHandlers cardHndlrs;
    CardTableHandler cardTblHndlr;
    GameController gameCntrllr;
    TurnController whoTurn;
    ValueController valueCntrllr;

    Transform cardTable;

    int drawCtr;
    int nothingCtr;
    int passCtr;
    public int algorithm1PasCnt;
    public int algorithm2PasCnt;

    private void Start()
    {
        cardHndlrs = GameObject.Find("CardHandler").GetComponent<CardHandlers>();
        cardTblHndlr = GameObject.Find("CardTableHandler").GetComponent<CardTableHandler>();
        gameCntrllr = GameObject.Find("GameController").GetComponent<GameController>();
        whoTurn = GameObject.Find("TurnController").GetComponent<TurnController>();
        valueCntrllr = GameObject.Find("ValueController").GetComponent<ValueController>();

        cardTable = GameObject.Find("CardTable").GetComponent<Transform>();

        nothingCtr = 0;
        passCtr = 0;
        algorithm1PasCnt = 0;
        algorithm2PasCnt = 0;
    }

    // Greedy Sort
    public List<Kartu> AIGreedySort(List<Kartu> greedyInput)
    {
        List<Kartu> greedyOutput = new List<Kartu>();

        greedyOutput = UrutBawah(greedyInput);
        greedyOutput = UrutJumlah(greedyOutput);

        return greedyOutput;
    }

    public List<Kartu> AIDnCSort(List<Kartu> dncInput)
    {
        List<Kartu> dncOutput = new List<Kartu>();

        dncOutput = UrutBawah(dncInput);
        dncOutput = UrutJumlah(dncInput);
        dncOutput = UrutKembar(dncInput);

        return dncOutput;
    }

    // Mengurutkan dari yang paling besar jumlah top dan bot value
    private List<Kartu> UrutJumlah(List<Kartu> input)
    {
        List<Kartu> output = new List<Kartu>();
        int jumlahCard = input.Count;
        int a, jum1, jum2 = 0;

        do
        {
            a = 0;

            for (int i = 0; i < (jumlahCard - 1); i++)
            {
                jum1 = input[i].topValue + input[i].botValue;
                jum2 = input[i + 1].topValue + input[i + 1].botValue;

                if (jum1 < jum2)
                {
                    Kartu gelas;
                    gelas = input[i];
                    input[i] = input[i + 1];
                    input[i + 1] = gelas;
                    a = a + 1;
                }
            }
        }
        while (a != 0);

        output = input;

        return output;
    }

    // Mengurutkan dari jumlah bot value yang paling besar
    private List<Kartu> UrutBawah(List<Kartu> input)
    {
        List<Kartu> output = new List<Kartu>();
        int jumlahCard = input.Count;
        int a, jum1, jum2 = 0;

        do
        {
            a = 0;

            for (int i = 0; i < (jumlahCard - 1); i++)
            {
                jum1 = input[i].botValue;
                jum2 = input[i + 1].botValue;

                if (jum1 < jum2)
                {
                    Kartu gelas;
                    gelas = input[i];
                    input[i] = input[i + 1];
                    input[i + 1] = gelas;
                    a = a + 1;
                }
            }
        }
        while (a != 0);

        output = input;

        return output;
    }

    // Mengurutkan dari yang sama dulu, besar kecilnya nilai gak dihitung
    private List<Kartu> UrutKembar(List<Kartu> input)
    {
        List<Kartu> output = new List<Kartu>();
        int jumlahCard = input.Count;
        int a = 0;
        bool kar1, kar2 = false;

        do
        {
            a = 0;

            for (int i = 0; i < (jumlahCard - 1); i++)
            {
                kar1 = input[i].topValue == input[i].botValue;
                kar2 = input[i + 1].topValue == input[i + 1].botValue;

                if (kar1 != kar2)
                {
                    if (kar1 == false)
                    {
                        Kartu gelas;
                        gelas = input[i];
                        input[i] = input[i + 1];
                        input[i + 1] = gelas;
                        a = a + 1;
                    }
                }
            }
        }
        while (a != 0);

        output = input;

        return output;
    }

    public void ValueCheckAndAct(List<Kartu> _AICard_) 
    {
        drawCtr = _AICard_.Count;
        nothingCtr = 0;

        for(int i = 0; i < _AICard_.Count; i++) 
        {
            if (_AICard_[i].topValue == valueCntrllr.valueTop || _AICard_[i].topValue == valueCntrllr.valueBot || _AICard_[i].botValue == valueCntrllr.valueTop || _AICard_[i].botValue == valueCntrllr.valueBot) 
            {
                if(_AICard_[i].topValue == valueCntrllr.valueTop && _AICard_[i].botValue == valueCntrllr.valueBot || _AICard_[i].topValue == valueCntrllr.valueBot && _AICard_[i].botValue == valueCntrllr.valueTop) 
                {
                    if(_AICard_[i].topValue == valueCntrllr.valueTop && _AICard_[i].botValue == valueCntrllr.valueBot) 
                    {
                        cardTblHndlr.addCardToBotList(_AICard_[i]);
                        cardTblHndlr.InstantiateBotCardSwap(cardTblHndlr.botCardOnTable, cardTable);
                        //cardTblHndlr.InstantiateBotCard(cardTblHndlr.botCardOnTable, cardTable);

                        DetailPanelController.InstantiateTheText(_AICard_[i].name + " Dikeluarkan Di Bot");
                        Debug.LogFormat(_AICard_[i] + " bot value = " + _AICard_[i].botValue + " sama dengan bot value " + valueCntrllr.valueBot);

                        valueCntrllr.ChangeBotValue(_AICard_[i].topValue);
                    }

                    else if (_AICard_[i].topValue == valueCntrllr.valueBot && _AICard_[i].botValue == valueCntrllr.valueTop) 
                    {
                        cardTblHndlr.addCardToTopList(_AICard_[i]);
                        cardTblHndlr.InstantiateTopCard(cardTblHndlr.topCardOnTable, cardTable);

                        DetailPanelController.InstantiateTheText(_AICard_[i].name + " Dikeluarkan Di Top");
                        Debug.LogFormat(_AICard_[i] + " bot value = " + _AICard_[i].botValue + " sama dengan top value " + valueCntrllr.valueTop);

                        valueCntrllr.ChangeTopValue(_AICard_[i].topValue);
                    }
                }

                else if (_AICard_[i].topValue == valueCntrllr.valueTop)       // Kartu top = top Value
                {
                    cardTblHndlr.addCardToTopList(_AICard_[i]);
                    cardTblHndlr.InstantiateTopCardSwap(cardTblHndlr.topCardOnTable, cardTable);
                    //cardTblHndlr.InstantiateTopCard(cardTblHndlr.topCardOnTable, cardTable);

                    DetailPanelController.InstantiateTheText(_AICard_[i].name + " Dikeluarkan Di Top");
                    Debug.LogFormat(_AICard_[i] + " top value = " + _AICard_[i].topValue + " sama dengan top value " + valueCntrllr.valueTop);

                    valueCntrllr.ChangeTopValue(_AICard_[i].botValue);
                }

                else if (_AICard_[i].topValue == valueCntrllr.valueBot)       // Kartu top = bot Value
                {
                    cardTblHndlr.addCardToBotList(_AICard_[i]);
                    cardTblHndlr.InstantiateBotCard(cardTblHndlr.botCardOnTable, cardTable);

                    DetailPanelController.InstantiateTheText(_AICard_[i].name + " Dikeluarkan Di Bot");
                    Debug.LogFormat(_AICard_[i] + " top value = " + _AICard_[i].topValue + " sama dengan bot value " + valueCntrllr.valueBot);

                    valueCntrllr.ChangeBotValue(_AICard_[i].botValue);                    
                }

                else if (_AICard_[i].botValue == valueCntrllr.valueTop)       // Kartu bot = top Value
                {
                    cardTblHndlr.addCardToTopList(_AICard_[i]);
                    cardTblHndlr.InstantiateTopCard(cardTblHndlr.topCardOnTable, cardTable);

                    DetailPanelController.InstantiateTheText(_AICard_[i].name + " Dikeluarkan Di Top");
                    Debug.LogFormat(_AICard_[i] + " bot value = " + _AICard_[i].botValue + " sama dengan top value " + valueCntrllr.valueTop);

                    valueCntrllr.ChangeTopValue(_AICard_[i].topValue);                    
                }

                else if (_AICard_[i].botValue == valueCntrllr.valueBot)       // Kartu bot = bot Value
                {
                    cardTblHndlr.addCardToBotList(_AICard_[i]);
                    cardTblHndlr.InstantiateBotCardSwap(cardTblHndlr.botCardOnTable, cardTable);
                    //cardTblHndlr.InstantiateBotCard(cardTblHndlr.botCardOnTable, cardTable);

                    DetailPanelController.InstantiateTheText(_AICard_[i].name + " Dikeluarkan Di Bot");
                    Debug.LogFormat(_AICard_[i] + " bot value = " + _AICard_[i].botValue + " sama dengan bot value " + valueCntrllr.valueBot);

                    valueCntrllr.ChangeBotValue(_AICard_[i].topValue);                    
                }

                if (whoTurn.algorithm1Turn) 
                {
                    cardHndlrs.algorithm1Card.Remove(cardHndlrs.algorithm1Card[i]);
                }

                else if (whoTurn.algorithm2Turn) 
                {
                    cardHndlrs.algorithm2Card.Remove(cardHndlrs.algorithm2Card[i]);
                }

                passCtr = 0;
                return;
            }

            else 
            {
                Debug.LogFormat("Baris " + i + "Tidak ada yang sama");

                if(i == (drawCtr-1) && _AICard_.Contains(_AICard_[i])) 
                {
                    gameCntrllr.plsDraw = true;
                }

                nothingCtr++;

                if (nothingCtr == _AICard_.Count)
                {
                    passCtr++;

                    DetailPanelController.InstantiateTheText("Tidak Bisa Mengeluarkan Kartu");

                    if (passCtr % 2 == 0) 
                    {
                        if (whoTurn.algorithm1Turn)
                        {
                            DetailPanelController.InstantiateTheText("Lewati Putaran");

                            algorithm1PasCnt++;
                            passCtr = 0;
                        }

                        if (whoTurn.algorithm2Turn) 
                        {
                            DetailPanelController.InstantiateTheText("Lewati Putaran");

                            algorithm2PasCnt++;
                            passCtr = 0;
                        }
                    }
                }
            }
        }
    }
}