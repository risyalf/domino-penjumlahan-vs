﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    AIHandler aiHndlr_;
    CardTableHandler cardTblHndlr;
    CardHandlers crdHndlr;                  // controlling isResetCardhand
    GameObject cardAreaGreedy;
    TurnController turnCntrllr;

    List<Kartu> greedyAICards = new List<Kartu>();
    List<Kartu> dncAICards = new List<Kartu>();
    
    void Start()
    {
        aiHndlr_ = GetComponent<AIHandler>();
        cardTblHndlr = GameObject.Find("CardTableHandler").GetComponent<CardTableHandler>();
        crdHndlr = GameObject.Find("CardHandler").GetComponent<CardHandlers>();
        cardAreaGreedy = GameObject.Find("CardAreaGreedy");
        turnCntrllr = GameObject.Find("TurnController").GetComponent<TurnController>();
    }

    public void AISortingController() 
    {
        if (GameController.bothPlay)
        {
            if (turnCntrllr.algorithm1Turn)
            {
                SortGreedy(crdHndlr.algorithm1Card);
                DetailPanelController.InstantiateTheText("Greedy Sorting");
            }

            if (turnCntrllr.algorithm2Turn)
            {
                SortDnC(crdHndlr.algorithm2Card);
                DetailPanelController.InstantiateTheText("DnC Sorting");
            }
        }

        else if (GameController.greedyPlay)
        {
            if (turnCntrllr.algorithm1Turn)
            {
                SortGreedy(crdHndlr.algorithm1Card);
                DetailPanelController.InstantiateTheText("Greedy1 Sorting");
            }

            if (turnCntrllr.algorithm2Turn)
            {
                SortGreedy(crdHndlr.algorithm2Card);
                DetailPanelController.InstantiateTheText("Greedy2 Sorting");
            }
        }

        else if (GameController.dncPlay)
        {
            if (turnCntrllr.algorithm1Turn)
            {
                SortDnC(crdHndlr.algorithm1Card);
                DetailPanelController.InstantiateTheText("DnC1 Sorting");
            }

            if (turnCntrllr.algorithm2Turn)
            {
                SortDnC(crdHndlr.algorithm2Card);
                DetailPanelController.InstantiateTheText("DnC2 Sorting");
            }
        }

        crdHndlr.ResetCardHand();
    }

    public void AIActionController() 
    {
        if (GameController.bothPlay)
        {
            if (turnCntrllr.algorithm1Turn)
            {
                GreedyAIAction(crdHndlr.algorithm1Card);
            }

            if (turnCntrllr.algorithm2Turn)
            {
                DnCAIAction(crdHndlr.algorithm2Card);
            }
        }

        else if (GameController.greedyPlay)
        {
            if (turnCntrllr.algorithm1Turn)
            {
                GreedyAIAction(crdHndlr.algorithm1Card);
            }

            else if (turnCntrllr.algorithm2Turn)
            {
                GreedyAIAction(crdHndlr.algorithm2Card);
            }
        }

        else if (GameController.dncPlay)
        {
            if (turnCntrllr.algorithm1Turn)
            {
                DnCAIAction(crdHndlr.algorithm1Card);
            }

            else if (turnCntrllr.algorithm2Turn)
            {
                DnCAIAction(crdHndlr.algorithm2Card);
            }
        }

        crdHndlr.ResetCardHand();
    }

    public void SortGreedy(List<Kartu> greedyCards)
    {
        greedyAICards = aiHndlr_.AIGreedySort(greedyCards);
        greedyCards = greedyAICards;
    }

    public void SortDnC(List<Kartu> dncCards)
    {
        dncAICards = aiHndlr_.AIDnCSort(dncCards);
        dncCards = dncAICards;
    }

    public void GreedyAIAction(List<Kartu> greedyCards) 
    {
        greedyAICards = greedyCards;
        aiHndlr_.ValueCheckAndAct(greedyAICards);
        greedyCards = greedyAICards;
    }

    void DnCAIAction(List<Kartu> dncCards)
    {
        dncAICards = dncCards;
        aiHndlr_.ValueCheckAndAct(dncAICards);
        dncCards = dncAICards;
    }
}
