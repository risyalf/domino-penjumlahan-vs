﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollButton : MonoBehaviour
{
    CardHandler cardHandler;

    Transform content;
    GameObject beginningContentPos;

    Transform cardTable;

    Transform beginningPos;
    public Transform rightLimit;
    public Transform leftLimit;

    CardPosition limitTopBot;
    CardHandler limitRightLeft;

    TurnController gameController;

    public bool isResetLimitPos;
    public bool rightLimitIncrease;

    private void Start()
    {
        beginningPos = GameObject.Find("BeginningPos").GetComponent<Transform>();
        rightLimit = GameObject.Find("RightLimit").GetComponent<Transform>();
        leftLimit = GameObject.Find("LeftLimit").GetComponent<Transform>();

        limitRightLeft = GameObject.Find("CardHandler").GetComponent<CardHandler>();

        cardHandler = GameObject.Find("CardHandler").GetComponent<CardHandler>();

        gameController = GameObject.Find("GameController").GetComponent<TurnController>();

        content = GameObject.Find("Content").GetComponent<Transform>();
        cardTable = GameObject.Find("ScrollTableCard").GetComponent<Transform>();

        limitTopBot = GameObject.Find("CardPosition").GetComponent<CardPosition>();

        beginningContentPos = new GameObject();
        beginningContentPos.transform.position = content.position;

        rightLimit.position = new Vector2(beginningPos.position.x + (5.9f * 7), beginningPos.position.y);
        leftLimit.position = new Vector2(beginningPos.position.x - 3, beginningPos.position.y);
    }

    private void Update()
    {
        if (isResetLimitPos)
        {
            if (gameController.algorithm2Turn)
            {
                rightLimit.position = new Vector2(beginningPos.position.x + (5.9f * limitRightLeft.greedyCards.Count), beginningPos.position.y);
            }

            if (gameController.algorithm1Turn)
            {
                rightLimit.position = new Vector2(beginningPos.position.x + (5.9f * limitRightLeft.dncCards.Count), beginningPos.position.y);
            }

            leftLimit.position = new Vector2(beginningPos.position.x - 3, beginningPos.position.y);
            content.position = beginningContentPos.transform.position;

            isResetLimitPos = false;
        }

        if (rightLimitIncrease)
        {
            rightLimit.position = new Vector2(rightLimit.position.x + 5.9f, beginningPos.position.y);
            rightLimitIncrease = false;
        }
    }

    public void UpScroll()
    {
        if (limitTopBot.topCanScroll)
        {
                cardTable.position = new Vector2(0, 0);
                cardTable.position = new Vector2(0, cardTable.position.y - (limitTopBot.limitTopCount * 6.1f));

                limitTopBot.topCanScroll = false;
                limitTopBot.botCanScroll = true;
        }
    }

    public void DownScroll()
    {
        if (limitTopBot.botCanScroll)
        {
                cardTable.position = new Vector2(0,0);
                cardTable.position = new Vector2(0, cardTable.position.y + (limitTopBot.limitBotCount * 6.1f));

                limitTopBot.topCanScroll = true;
                limitTopBot.botCanScroll = false;
        }
    }

    public void RightScroll()
    {
        if (leftLimit.position.x > -13)
        {
            return;
        }

        else
        {
            rightLimit.position = new Vector2(rightLimit.position.x + 5, rightLimit.position.y);
            leftLimit.position = new Vector2(leftLimit.position.x + 5, leftLimit.position.y);
            content.position = new Vector2(content.position.x + 5, content.position.y);
        }
    }

    public void LeftScroll()
    {
        if (rightLimit.position.x < 10)
        {
            return;
        }

        else
        {
            rightLimit.position = new Vector2(rightLimit.position.x - 5, rightLimit.position.y);
            leftLimit.position = new Vector2(leftLimit.position.x - 5, leftLimit.position.y);
            content.position = new Vector2(content.position.x - 5, content.position.y);
        }

        /*if (gameController.p1)
        {
            //rightLimit.position = new Vector2(beginningPos.position.x + (6f * limitRightLeft.player1Card.Count), rightLimit.position.y);
            rightLimit.position = new Vector2(rightLimit.position.x - 6, rightLimit.position.y);
            content.position = new Vector2(content.position.x - 5, content.position.y);
        }

        if (gameController.p2)
        {
            //rightLimit.position = new Vector2(rightLimit.position.x + (6f * limitRightLeft.player1Card.Count), rightLimit.position.y);
            rightLimit.position = new Vector2(rightLimit.position.x - 6, rightLimit.position.y);
            content.position = new Vector2(content.position.x - 5, content.position.y);
        } */
    }
}
