﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollRectManager : MonoBehaviour
{
    public bool stopScrolling;

    void Start()
    {
        stopScrolling = false;
    }

    private void Update()
    {
        if (stopScrolling)
        {
            GetComponent<UnityEngine.UI.ScrollRect>().enabled = false;
        }

        else if (stopScrolling == false)
        {
            GetComponent<UnityEngine.UI.ScrollRect>().enabled = true;
        }
    }
}
