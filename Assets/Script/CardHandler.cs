﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardHandler : MonoBehaviour
{
    public List<Kartu> deck = new List<Kartu>();
    public List<Kartu> drawableCard = new List<Kartu>();

    public List<Kartu> greedyCards = new List<Kartu>();
    public List<Kartu> dncCards = new List<Kartu>();

    public List<Kartu> AISort = new List<Kartu>();

    TurnController gameController;

    ValueController valueController;

    int randomNumber;
    //int i, j, k;

    //DIATAS ADALAH VARIABLE DRAWING CARD
    //DIBAWAH ADALAH VARIABLE PELETAKAN KARTU DI KARTU TANGAN
    Kartu chosenCard;

    Kartu firstCardOnTable;
    GameObject firstTableCardPos;

    //public Transform addedCardPosition;
    public Transform beginningPos;
    Transform theParentToDestroy;

    GameObject firstCardParent;

    public GameObject cardParent;       //content
    public GameObject parentPos;

    float distanceBetween;
    
    public bool isResetCardHand;
    public bool signalToCard;

    ScrollButton theScrollBtn;
    
    public void Start()
    {
        theScrollBtn = GameObject.Find("ScrollButton").GetComponent<ScrollButton>();

        firstTableCardPos = GameObject.Find("FirstCardPos");

        gameController = GameObject.Find("GameController").GetComponent<TurnController>();

        valueController = GameObject.Find("ValueController").GetComponent<ValueController>();

        isResetCardHand = false;
        
        theParentToDestroy = GameObject.Find("Content").GetComponent<Transform>();

        beginningPos.position = cardParent.transform.position;

        distanceBetween = 0;

        moveCardFromDeckToDrawableCard();

        gameController.WhoFirst();

        if (gameController.algorithm2Turn)
        {
            for (int i = 0; i < 7; i++)
            {
                AddCard();
            }

            for (int i = 0; i < 7; i++)
            {
                Suffle();
                dncCards.Add(drawableCard[randomNumber]);
                drawableCard.Remove(drawableCard[randomNumber]);
            }
        }

        else if (gameController.algorithm1Turn)
        {
            for (int i = 0; i < 7; i++)
            {
                AddCard();
            }

            for (int i = 0; i < 7; i++)
            {
                Suffle();
                greedyCards.Add(drawableCard[randomNumber]);
                drawableCard.Remove(drawableCard[randomNumber]);
            }
        }
    }

    private void Update()
    {
        if (isResetCardHand)
        {
            foreach (Transform child in theParentToDestroy)
            {
                Destroy(child.gameObject);
            }

            cardParent.transform.position = beginningPos.position;
            
            if (gameController.algorithm2Turn)
            {
                for (int i = 0; i < greedyCards.Count; i++)
                {
                    chosenCard = Instantiate(greedyCards[i], beginningPos.position, beginningPos.rotation) as Kartu;

                    chosenCard.transform.SetParent(cardParent.transform, true);

                    chosenCard.transform.position = new Vector3(beginningPos.transform.position.x + (i * distanceBetween), beginningPos.transform.position.y, 0);
                    
                    //chosenCard.GetCursorFollowOn();
                }
            }

            if (gameController.algorithm1Turn)
            {
                for (int i = 0; i < dncCards.Count; i++)
                {
                    chosenCard = Instantiate(dncCards[i], beginningPos.position, beginningPos.rotation) as Kartu;

                    chosenCard.transform.SetParent(cardParent.transform, true);

                    chosenCard.transform.position = new Vector3(beginningPos.transform.position.x + (i * distanceBetween), beginningPos.transform.position.y, 0);
                    
                    //chosenCard.GetCursorFollowOn();
                }
            }

            theScrollBtn.isResetLimitPos = true;
            isResetCardHand = false;
            signalToCard = true;
        }
    }
    public void ClickDraw()
    {
        if (drawableCard.Count > 0)
        {
            AddCard();

            Debug.Log("Sisa kartu adalah = " + drawableCard.Count);            
        }

        else 
        {
            Debug.Log("deck telah habis");
        }

        theScrollBtn.rightLimitIncrease = true;
    }

    int Suffle()
    {
        return randomNumber = Random.Range(0, drawableCard.Count);
    }
    //DIATAS ADALAH METHOD UNTUK SUFFLE deck

    public void AddCard()
    {
        Suffle();

        Debug.Log(drawableCard[randomNumber]);

        //addedCardPosition.position = parentPos.transform.position;

        drawableCard[randomNumber].GetValue();

        //DIBAWAH ADALAH PETELAKAN KARTU

        chosenCard = Instantiate(drawableCard[randomNumber]/*, startPosition.position, startPosition.rotation*/) as Kartu;

        chosenCard.transform.SetParent(cardParent.transform, false);

        RectTransform cardRT;
        cardRT = chosenCard.GetComponent<RectTransform>();

        cardRT.localPosition = new Vector3(35, 50, 0);
        cardRT.localScale = new Vector3(0.077f, 0.077f, 0);

        /*if (gameController.dncAI)
        {
            chosenCard.transform.position = new Vector3(addedCardPosition.transform.position.x + (greedyCards.Count * distanceBetween), addedCardPosition.transform.position.y, 0);
        }

        else if (gameController.greedyAI)
        {
            chosenCard.transform.position = new Vector3(addedCardPosition.transform.position.x + (dncCards.Count * distanceBetween), addedCardPosition.transform.position.y, 0);
        }

        addedCardPosition.transform.position = chosenCard.transform.position;*/

        distanceBetween = 6.35f;

        /*if (gameController.dncAI)
        {
            greedyCards.Add(drawableCard[randomNumber]);
        }

        else if (gameController.greedyAI)
        {
            dncCards.Add(drawableCard[randomNumber]);
        }*/
        
        drawableCard.Remove(drawableCard[randomNumber]);

        //chosenCard.GetCursorFollowOn();
    }

    void moveCardFromDeckToDrawableCard()
    {
        for (int i = 0; i < deck.Count; i++)
        {
            drawableCard.Add(deck[i]);
        }
    }

    public void FirstTableCard()
    {
        Suffle();

        firstCardParent = GameObject.Find("ScrollTableCard");

        firstCardOnTable = Instantiate(drawableCard[randomNumber]) as Kartu;

        firstCardOnTable.transform.SetParent(firstCardParent.transform);

        firstCardOnTable.transform.position = firstTableCardPos.transform.position;

        valueController.valueTop = firstCardOnTable.topValue;
        valueController.valueBot = firstCardOnTable.botValue;

        //firstCardOnTable.GetCursorFollowOff();
        
        drawableCard.Remove(drawableCard[randomNumber]);
    }

    public void ClickSortGreedy()
    {
        AIHandler AISort_ = new AIHandler();
        AISort = AISort_.AIGreedySort(greedyCards);
        greedyCards = AISort;
        isResetCardHand = true;
    }
}
